# WooCommerce


##  1) Installation 

### 1.1) Installer wordpress

    -- télécharger wordpress sur wordpress.org
    -- configurer vos admin et votre base de données.
    -- remplacer wp-content par celui du repo.

### 1.2) Installation des plugins 

    -- woocommerce
    -- stripe
    -- custom order status for woocommerce

## 2) Importer les configurations

    La configuration se trouve dans le fichier .xml fournis, 
    a l'intérieur les étapes d'importation sont précisées.

## 3) Configuration du paiement
    
    Créer un compte stripe et entrer les informations nécéssaires.
    Récupérer les clées "public" et "secret".

    Entrer les clées sur wordpress :
        extensions -> extensions installées -> WooCommerce Stripe Gateway ->
        Régagles -> Détails du compte -> entrer les clées de stripe.

    Le mode "test" de stripe permet de passer des commandes test quand ce denier est activé.

